module.exports = {
	// Global options:
	sourceDir: 'build/webpack',

	// Command options:
	build: {
		overwriteDest: true,
	},
}
